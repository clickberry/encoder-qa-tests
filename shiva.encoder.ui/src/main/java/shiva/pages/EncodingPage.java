package shiva.pages;

import java.io.File;

import shiva.util.PropertyLoader;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;

import org.openqa.selenium.By;
import org.testng.Reporter;
import org.junit.runner.*;
import org.junit.runners.*;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;

import org.junit.AfterClass;

import java.io.*;
import java.util.*;

import org.junit.Test;
import org.testng.annotations.*;
import org.testng.Assert.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
/*
public class EncodingPage {	
	
	private static SelenideElement FileSelectField = $(By.xpath("//input[@id='file']"));
	private static SelenideElement UploadVideoButton = $(By.xpath("//button[@class='btn btn-success']"));	
	private static SelenideElement EncodedVideoCode = $(By.xpath("//div[@id='html-code']"));	
	private static SelenideElement EncodedVideoLink = $(By.xpath("//div[@id='html-code']"));
	
	private StringBuffer verificationErrors = new StringBuffer();
	
@DataProvider
    public Object[][] testData() {
        return new Object[][] {
            new Object[] {"C:\\Java Luna\\encoder-qa-tests\\testdata\\Wildlife.wmv"},
            new Object[] {"C:\\Java Luna\\encoder-qa-tests\\testdata\\ve_step1.mp4"},           
    };
}

//@Test(dataProvider = "testData")
public void SelectVideo(File dataProvider="testData") {
	
	try{
		
		shiva.pages.IndexPage.OpenIndexPage();
		
		FileSelectField.uploadFile(TestVideo);		
	    
		sleep(2000); //debug
		
		UploadVideoButton.click();
		EncodedVideoCode.waitUntil(appear, 20000);
		sleep(5000); // debug	
		
		Reporter.log(EncodedVideoLink.getAttribute("innerHTML"));	
		
	}
	
	}

}

*/
