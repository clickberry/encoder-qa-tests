package shiva.pages;

import java.io.File;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Condition.*;

import org.openqa.selenium.By;
import org.testng.Reporter;

/**
 * Encoder UI Index Page.
 * 
 *
 */

public class IndexPage {
	
	private static SelenideElement PageTitle = $("img.brand");
	private static SelenideElement WikiLink = $(By.xpath("//a[@href='https://github.com/clickberry/video-encoder/wiki/Videos-API']"));
	private static SelenideElement GitLink = $(By.xpath("//footer/p/a[@href='https://github.com/clickberry/video-encoder']"));
	
	private static SelenideElement FileSelectField = $(By.xpath("//input[@id='file']"));
	private static SelenideElement UploadVideoButton = $(By.xpath("//button[@class='btn btn-success']"));	
	private static SelenideElement EncodedVideoCode = $(By.xpath("//div[@id='html-code']"));	
	private static SelenideElement EncodedVideoLink = $(By.xpath("//div[@id='html-code']"));
	
	private static SelenideElement CodeHTMLButton = $(By.xpath("//div[@class='btn-group']/a[@href='#html']"));
	private static SelenideElement CodeHTMLSample = $(By.xpath("//div[@id='html']"));
	
	private static SelenideElement CodeCurlButton = $(By.xpath("//div[@class='btn-group']/a[@href='#curl']"));
	private static SelenideElement CodeCurlSample = $(By.xpath("//div[@id='curl']"));
	
	private static SelenideElement CodeJavaScriptButton = $(By.xpath("//div[@class='btn-group']/a[@href='#javascript']"));
	private static SelenideElement CodeJavaScriptSample = $(By.xpath("//div[@id='javascript']"));
	
	private static SelenideElement CodePythonButton = $(By.xpath("//div[@class='btn-group']/a[@href='#python']"));
	private static SelenideElement CodePythonSample = $(By.xpath("//div[@id='python']"));
	
	private static SelenideElement CodeCSharpButton = $(By.xpath("//div[@class='btn-group']/a[@href='#csharp']"));
	private static SelenideElement CodeCSharpSample = $(By.xpath("//div[@id='csharp']"));	
	
	private static SelenideElement CodeNodeJSButton = $(By.xpath("//div[@class='btn-group']/a[@href='#nodejs']"));
	private static SelenideElement CodeNodeJSSample = $(By.xpath("//div[@id='nodejs']"));
	
	
/**
 * Open Index Page	
 */
	
public static void OpenIndexPage() {
	
	open(baseUrl + "/");	

}

/**
 * Wait until index page is loaded. 
 */

public static void waitUntilPagesIsLoaded() {
	  FileSelectField.shouldBe(visible);
	  UploadVideoButton.shouldBe(visible);
	  sleep(1000);
	  } 

	
	
/**
 * Check Elements on Index Page
 */
	
public static void CheckIndexPage() {	
	
	PageTitle.shouldBe(visible);
	WikiLink.shouldBe(visible);
	GitLink.shouldBe(visible);
	
	CodeHTMLButton.shouldBe(visible);
	CodeHTMLSample.shouldBe(visible);	
	
	CodeCurlButton.shouldBe(visible);
	CodeJavaScriptButton.shouldBe(visible);
	CodePythonButton.shouldBe(visible);
	CodeCSharpButton.shouldBe(visible);
	CodeNodeJSButton.shouldBe(visible);	
	
}

/**
 * Check Code samples
 */

public static void CheckCodeSamples() {
	
	CodeCurlButton.click();
	CodeCurlSample.should(appear);
	CodeHTMLSample.should(disappear);
	
	CodeJavaScriptButton.click();
	CodeJavaScriptSample.should(appear);
	CodeCurlSample.should(disappear);
	
	CodePythonButton.click();
	CodePythonSample.should(appear);
	CodeJavaScriptSample.should(disappear);
	
	CodeCSharpButton.click();
	CodeCSharpSample.should(appear);
	CodePythonSample.should(disappear);
	
	CodeNodeJSButton.click();
	CodeNodeJSSample.should(appear);
	CodeCSharpSample.should(disappear);
	
	CodeHTMLButton.click();
	CodeHTMLSample.should(appear);
	CodeNodeJSSample.should(disappear);
	
}

//--------------------------------------------- Encoding ---------------------------------------------------

/**
 * Select Video for Encoding
 */

public static void SelectVideo(File VideoFileUrl) {	

    FileSelectField.uploadFile(VideoFileUrl);		

    sleep(2000); //debug
	
}

/**
 * Upload File for encoding
 */

public static void VideoUploadPass() {
	
	UploadVideoButton.click();
	EncodedVideoCode.waitUntil(appear, 120000);
	sleep(5000); // debug	
	
	Reporter.log(EncodedVideoLink.getAttribute("innerHTML"));	
	
	
}

}
