package shiva.tests;

import java.io.File;

import org.testng.annotations.*;

/**
 * Video Encoding
 * 
 *
 */

public class EncodeVideoTest extends TestBase {
	
/**
 * Step1_Upload_MP4_2304p_10
 */
	
@Test

public void TestVideoEncoding_Step01() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step1.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step2_Upload_MP4_1080p_more_8000_kBps_8 
 */

@Test

public void TestVideoEncoding_Step02() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step2.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step3_Upload_MP4_1080p_less_8000_kBps_8 
 */

@Test

public void TestVideoEncoding_Step03() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step3.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step4_Upload_MP4_720p_more_5000_kBps_6
 */

@Test

public void TestVideoEncoding_Step04() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step4.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step5_Upload_MP4_720p_less_5000_kBps_6
 */

@Test

public void TestVideoEncoding_Step05() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step5.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step6_Upload_MP4_480p_more_2500_kBps_4
 */

@Test

public void TestVideoEncoding_Step06() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step6.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step7_Upload_MP4_480p_less_2500_kBps_4
 */

@Test

public void TestVideoEncoding_Step07() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step7.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step8_Upload_MP4_360p_more_1000_kBps_2
 */

@Test

public void TestVideoEncoding_Step08() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step8.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step9_Upload_MP4_360p_more_1000_kBps_2
 */

@Test

public void TestVideoEncoding_Step09() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step9.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step10_Upload_MP4_320x240_2
 */

@Test

public void TestVideoEncoding_Step10() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step10.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step11_Upload_FLV_2304p_10
 */

@Test

public void TestVideoEncoding_Step11() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step11.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step12_Upload_FLV_1080p_more_8000_kBps_8
 */

@Test

public void TestVideoEncoding_Step12() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step12.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step13_Upload_FLV_1080p_less_8000_kBps_8
 */

@Test

public void TestVideoEncoding_Step13() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step13.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step14_Upload_FLV_720p_more_5000_kBps_6
 */

@Test

public void TestVideoEncoding_Step14() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step14.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step15_Upload_FLV_720p_less_5000_kBps_6
 */

@Test

public void TestVideoEncoding_Step15() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step15.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step16_Upload_FLV_480p_more_2500_kBps_4
 */

@Test

public void TestVideoEncoding_Step16() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step16.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step17_Upload_FLV_480p_less_2500_kBps_4
 */

@Test

public void TestVideoEncoding_Step17() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step17.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step18_Upload_FLV_360p_more_1000_kBps_2
 */

@Test

public void TestVideoEncoding_Step18() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step18.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step19_Upload_FLV_360p_less_1000_kBps_2
 */

@Test

public void TestVideoEncoding_Step19() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step19.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step20_Upload_FLV_320x240_2
 */

@Test

public void TestVideoEncoding_Step20() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step20.flv"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step21_Upload_WEBM_2304p_10
 */

@Test

public void TestVideoEncoding_Step21() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step21.webm"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step23_Upload_WEBM_1080p_less_8000_kBps_8
 */

@Test

public void TestVideoEncoding_Step23() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step23.webm"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step24_Upload_WEBM_720p_more_5000_kBps_6
 */

@Test

public void TestVideoEncoding_Step24() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step24.webm"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step25_Upload_WEBM_720p_less_5000_kBps_6
 */

@Test

public void TestVideoEncoding_Step25() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step25.webm"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step26_Upload_WEBM_480p_more_2500_4
 */

@Test

public void TestVideoEncoding_Step26() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step26.webm"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step27_Upload_WEBM_480p_less_2500_4
 */

@Test

public void TestVideoEncoding_Step27() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step27.webm"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step28_Upload_WEBM_360p_more_1000_2
 */

@Test

public void TestVideoEncoding_Step28() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step28.webm"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step29_Upload_WEBM_360p_less_1000_2
 */

@Test

public void TestVideoEncoding_Step29() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step29.webm"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step30_Upload_WEBM_320x240_2
 */

@Test

public void TestVideoEncoding_Step30() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step30.webm"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step31_Upload_MOV_2304p_10
 */

@Test

public void TestVideoEncoding_Step31() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step31.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step32_Upload_MOV_1080p_more_8000_kBps_8
 */

@Test

public void TestVideoEncoding_Step32() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step32.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step33_Upload_MOV_1080p_less_8000_kBps_8
 */

@Test

public void TestVideoEncoding_Step33() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step33.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step34_Upload_MOV_720p_more_5000_kBps_6
 */

@Test

public void TestVideoEncoding_Step34() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step34.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step35_Upload_MOV_720p_less_5000_kBps_6
 */

@Test

public void TestVideoEncoding_Step35() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step35.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step36_Upload_MOV_960x540_4
 */

@Test

public void TestVideoEncoding_Step36() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step36.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step37_Upload_MOV_480p_more_2500_kBps_4
 */

@Test

public void TestVideoEncoding_Step37() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step37.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step38_Upload_MOV_480p_less_2500_kBps_4
 */

@Test

public void TestVideoEncoding_Step38() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step38.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step39_Upload_MOV_360p_more_1000_kBps_2
 */

@Test

public void TestVideoEncoding_Step39() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step39.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step40_Upload_MOV_360p_less_1000_kBps_2
 */

@Test

public void TestVideoEncoding_Step40() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step40.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step41_Upload_MOV_320x240_2
 */

@Test

public void TestVideoEncoding_Step41() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step41.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step42_Upload_video_with_1_audio_channel_2
 */

@Test

public void TestVideoEncoding_Step42() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step42.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step43_Upload_video_with_2_audio_channel_2
 */

@Test

public void TestVideoEncoding_Step43() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step43.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step44_Upload_video_with_6_audio_channel_2
 */

@Test

public void TestVideoEncoding_Step44() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step44.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step45_Upload_video_with_fps_more_60_2
 */

@Test

public void TestVideoEncoding_Step45() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step45.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step46_Upload_video_with_fps_less_10_2
 */

@Test

public void TestVideoEncoding_Step46() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step46.mp4"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step48_Upload_video_MOV_with_rotation_90_6
 */

@Test

public void TestVideoEncoding_Step48() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step48.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step49_Upload_video_MOV_with_rotation_180_6
 */

@Test

public void TestVideoEncoding_Step49() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step49.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

/**
 * Step50_Upload_video_MOV_with_rotation_270_6
 */

@Test

public void TestVideoEncoding_Step50() {
	
	shiva.pages.IndexPage.OpenIndexPage();	
	
	File VideoFileUrl = new File("\\Java Luna\\encoder-qa-tests\\testdata\\ve_step50.mov"); 
		
	shiva.pages.IndexPage.SelectVideo(VideoFileUrl);
	
	shiva.pages.IndexPage.VideoUploadPass();	
}

}
