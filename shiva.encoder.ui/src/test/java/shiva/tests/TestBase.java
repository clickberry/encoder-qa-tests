package shiva.tests;

import org.junit.AfterClass;
import org.junit.Rule;
import org.testng.annotations.BeforeTest;

import shiva.util.PropertyLoader;
import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;

import com.codeborne.selenide.junit.ScreenShooter;

/**
 * Base class with configuration settings.
 *
 */

public abstract class TestBase { 
	
  /**
   * Make screenshot if test fails.	
   */
	
  @Rule	
	
  public ScreenShooter screenShooter = ScreenShooter.failedTests();  
  
  /**
   *  Start WebDriver with configuration from POM profile
   */

  @BeforeTest    
  
  public static void setUp() { 	  
	  
	  baseUrl = PropertyLoader.loadProperty("site.url");
	  
	  browser = PropertyLoader.loadProperty("browser.name");
			  
	  open(baseUrl + "/");		 
	    
  }   
   
  /**
   * Quit WebDriver after all tests execution.
   */
  
  @AfterClass       
      
  public static void close() {  	  
	    closeWebDriver();	    
	  }   
 
  
}
