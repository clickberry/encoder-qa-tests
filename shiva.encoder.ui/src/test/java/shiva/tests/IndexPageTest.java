package shiva.tests;

import org.testng.annotations.*;

/**
 * Tests for Index Page elements.
 * 
 *
 */

public class IndexPageTest extends TestBase {
	
/**
 * Test data on Index Page	
 */
	
@Test

public void TestIndexPage() {
	
shiva.pages.IndexPage.OpenIndexPage();

shiva.pages.IndexPage.CheckIndexPage();

shiva.pages.IndexPage.CheckCodeSamples();
	
	
}

}
